---
title: "About"
description: ""
featured_image: ''
menu:
  main:
    weight: 1
---
Rich Holmes is a person, which is more than you can say about a lot of entities online. He was a physicist until he retired. He still gets out of bed and does things, such as synth DIY, recreational mathematics, morris dancing, casual bicycling, and walking virtual tours of far off places.
