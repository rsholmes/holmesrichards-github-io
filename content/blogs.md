---
title: Blogs
featured_image: ''
omit_header_text: true
description: Rich Holmes's blogs
type: page
menu: main

---

* [Doctroidal Dissertations](https://doctroid.richholmes.xyz) — General personal blog
* [Walking In Space](https://walkinginspace.richholmes.xyz) — Walking the worlds, one at a time
* [Analog Output](https://analogoutput.richholmes.xyz) — Synth DIY for big knob lovers
* [Mathematrec](https://mathematrec.richholmes.xyz) — Mathematical recreations
